package com.zsl.adapter;

import java.util.List;

import org.jivesoftware.smack.RosterEntry;

import com.zsl.bean.Msg;
import com.zsl.myqq.LoginActivity;
import com.zsl.myqq.R;
import com.zsl.smack.Smack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MsgAdapter extends BaseAdapter {
	private List<Msg> msgList;
	private LayoutInflater inflater;
	private Context context;
	private Smack smack=LoginActivity.getSmack();
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return msgList.size();
	}

	public MsgAdapter(List<Msg> msgList, Context context) {
		super();
		this.msgList = msgList;
		this.context = context;
		this.inflater=inflater.from(context);
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return msgList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}
	
	public class ViewHolder{
		private TextView tv_name;
		private TextView tv_count;
	}
	@Override
	public View getView(int arg0, View srcoview, ViewGroup arg2) {
		// TODO Auto-generated method stub
		ViewHolder holder=null;
		srcoview=inflater.inflate(R.layout.msg_item, null);
		holder=new ViewHolder();
		holder.tv_name=(TextView) srcoview.findViewById(R.id.recent_list_item_name);
		holder.tv_count=(TextView) srcoview.findViewById(R.id.unreadmsg);
		srcoview.setTag(holder);
		Msg msg=msgList.get(arg0);
		StringBuffer buffer=new StringBuffer(msg.getMfrom());
		String fname=buffer.substring(0, buffer.indexOf("@"));
		String count=msg.getMsg();
		holder.tv_name.setText(fname);
		holder.tv_count.setText(count);
		return srcoview;
	}

}
