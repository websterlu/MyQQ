package com.zsl.adapter;

import java.util.HashMap;
import java.util.List;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChatAdapter extends BaseAdapter {
	private List<HashMap<String, Object>> msgList;
	private int []layout;
	private int []layout_id;
	private LayoutInflater inflater;
	private Context context;
	
	public ChatAdapter(List<HashMap<String, Object>> megList, int[] layout,
			int[] layout_id,Context context) {
		super();
		this.msgList = megList;
		this.layout = layout;
		this.layout_id = layout_id;
		this.inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return msgList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return msgList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}
	public class ViewHolder{
		private ImageView iv_icon;
		private TextView tv_msg;
		private TextView tv_time;
	}
	@Override
	public View getView(int arg0, View srcoview, ViewGroup arg2) {
		// TODO Auto-generated method stub
		ViewHolder holder=null;
		int who=(Integer) msgList.get(arg0).get("who");
		srcoview=inflater.inflate(layout[who==0?0:1], null);
		holder=new ViewHolder();
		holder.iv_icon=(ImageView) srcoview.findViewById(layout_id[who*3]);
		holder.tv_msg=(TextView) srcoview.findViewById(layout_id[who*3+1]);
		holder.tv_time=(TextView) srcoview.findViewById(layout_id[who*3+2]);
		
		srcoview.setTag(holder);
		holder.iv_icon.setImageResource((Integer) msgList.get(arg0).get("image"));
		holder.tv_msg.setText(msgList.get(arg0).get("msg").toString());
		holder.tv_time.setText(msgList.get(arg0).get("time").toString());
		
		return srcoview;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

}
