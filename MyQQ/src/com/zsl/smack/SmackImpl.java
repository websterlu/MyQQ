package com.zsl.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.ReportedData;
import org.jivesoftware.smackx.ReportedData.Row;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.search.UserSearchManager;


public class SmackImpl implements Smack {
	private String serverIP="10.0.2.2";
	private int serverport=5222;
	private ConnectionConfiguration configuration;//连接配置 
	private XMPPConnection xmppConnection; //连接对象
	
	
	public SmackImpl() {
		/* 创建服务器连接对象*/
		configuration=new ConnectionConfiguration(serverIP, serverport);
		configuration.setSASLAuthenticationEnabled(false);
		xmppConnection=new XMPPConnection(configuration);
	}
	
	/**
	 * 重新连接
	 * @return
	 */
	public XMPPConnection reConnection(){
		if (xmppConnection.isConnected()) {
			xmppConnection.disconnect();
		}
		
		try {
			xmppConnection.connect();
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			
			return null;
		}
		return xmppConnection;
	}

	@Override
	public boolean login(String name, String pass) throws XMPPException {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			xmppConnection.disconnect();
		}
		
		xmppConnection.connect();
		xmppConnection.login(name, pass);
		return xmppConnection.isAuthenticated();
	}

	@Override
	public boolean logout() {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			new Thread(){
				public void run() {
					xmppConnection.disconnect();
				};
				
			}.start();
		}
		return true;
	}
	
	@Override
	public boolean isAuthenticated() {
		// TODO Auto-generated method stub
		if (xmppConnection!=null) {
			return (xmppConnection.isConnected()&&xmppConnection.isAuthenticated());
		}
		return false;
	}
	
	@Override
	public int register(String name,String pass) {
		reConnection();
		if (xmppConnection.isConnected()) {
			System.out.println("==========啊啊啊========");
			Registration reg=new Registration();
			reg.setType(IQ.Type.SET);
			reg.setTo(xmppConnection.getServiceName());
			reg.setUsername(name);
			reg.setPassword(pass);
			//标记为安卓设备
			reg.addAttribute("android", "geolo_createUser_android");
			PacketFilter filter=new AndFilter(new PacketIDFilter(reg.getPacketID()),new PacketTypeFilter(IQ.class));
			PacketCollector collector=xmppConnection.createPacketCollector(filter);
			
			xmppConnection.sendPacket(reg);
			IQ result=(IQ) collector.nextResult(SmackConfiguration.getPacketReplyTimeout());
			collector.cancel();
			
			if (result==null) {
				return 0;
			} else if(result.getType() == IQ.Type.RESULT){
				return 1;
			} else{
				if (result.getError().toString().equalsIgnoreCase("conflict(409)")) {
					return 2;
				} else {
					return 3;
				}
			}
			
		}
		return 0;
	}

	
	@Override
	public List<RosterGroup> getRosterGroup() {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			
			List<RosterGroup> rgList=new ArrayList<RosterGroup>();
			Collection<RosterGroup> groups=xmppConnection.getRoster().getGroups();
			Iterator<RosterGroup> i=groups.iterator();
			while (i.hasNext()) {
				rgList.add(i.next());
				
			}
			for (int j = 0; j < rgList.size(); j++) {
				System.out.println("========:"+rgList.get(j).getName());
			}
			return rgList;
		}
		
		return null;
	}

	@Override
	public List<RosterEntry> getEntriesByGroup(String groupName) {
		// TODO Auto-generated method stub
		 if (xmppConnection.isConnected()) {
			

		 List<RosterEntry> Entrieslist = new ArrayList<RosterEntry>();  

		 RosterGroup rosterGroup = xmppConnection.getRoster().getGroup(groupName);

		 Collection<RosterEntry> rosterEntry = rosterGroup.getEntries(); 

		 Iterator<RosterEntry> i = rosterEntry.iterator();  

		 while (i.hasNext()) {  

		 Entrieslist.add(i.next());  

		 }  
		 return Entrieslist; 
		 }
		 return null;
	}
	@Override
	public XMPPConnection getConnection() {
		if (xmppConnection.isConnected()) {
			return xmppConnection;
		}else{
			try {
				xmppConnection.connect();
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return xmppConnection;
		
	}

	@Override
	public boolean addGroup(String groupName) {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			try {
				xmppConnection.getRoster().createGroup(groupName);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
		}
		return false;
	}

	@Override
	public boolean addRoster(String user, String name, String groupName) {
		if (xmppConnection.isConnected()) {
			if (groupName==null) {
				groupName="我的好友";
			}
			if ("".equals(name)) {
				name=null;
			}
			if ("".equals(user)||user.equals(null)) {
				return false;
			}
			try {
				xmppConnection.getRoster().createEntry(user+"@"+xmppConnection.getServiceName().toString(), name, new String []{groupName});
			} catch (XMPPException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	@Override
	public List<HashMap<String, Object>> searchUser(String name) {
		HashMap<String , Object> user=null;
		List<HashMap<String , Object>> results=null;
		if (xmppConnection.isConnected()) {
			try {
				new ServiceDiscoveryManager(xmppConnection);
				UserSearchManager usManager =new UserSearchManager(xmppConnection);
				String serverName=xmppConnection.getServiceName();
				System.out.println("============serverName:"+serverName);
				Form searchForm =usManager.getSearchForm(serverName);
				Form answerForm =searchForm.createAnswerForm();
				answerForm.setAnswer("userAccount", true);            
				answerForm.setAnswer("userPhote", name);
				ReportedData reportedData=usManager.getSearchResults(answerForm, "search"+serverName);
				Iterator<Row> i=reportedData.getRows();
				new ArrayList<HashMap<String,Object>>();
				Row row=null;
				while (i.hasNext()) {
					user=new HashMap<String, Object>();
					row=i.next();
					user.put("userAccount", row.getValues("userAccount").next().toString());
					user.put("userPhote", row.getValues("userPhote").next().toString());
					results.add(user);
				}
				
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return results;
	}

	@Override
	public boolean getGroup(String Gname) {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			RosterGroup rosterGroup=xmppConnection.getRoster().getGroup(Gname);
			if (rosterGroup!=null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean changePwd(String pwd) {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			try {
				xmppConnection.getAccountManager().changePassword(pwd);
				return true;
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public VCard getVCard(String user) {
		// TODO Auto-generated method stub
		VCard card=new VCard();
		try {
			card.load(xmppConnection, user);
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return card;
	}

	@Override
	public List<RosterEntry> getEntries() {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			Collection<RosterEntry> entries=xmppConnection.getRoster().getEntries();
			Iterator<RosterEntry> i=entries.iterator();
			List<RosterEntry> Entrieslist = new ArrayList<RosterEntry>(); 
			while (i.hasNext()) {
				Entrieslist.add(i.next());
			}
			return Entrieslist;
		}
		return null;
	}

	@Override
	public RosterEntry getRosterEntry(String name) {
		// TODO Auto-generated method stub
		if (xmppConnection.isConnected()) {
			return xmppConnection.getRoster().getEntry(name);
		}
		return null;
	}

}
