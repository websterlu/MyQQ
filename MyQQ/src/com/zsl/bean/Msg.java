package com.zsl.bean;

import java.io.Serializable;

public class Msg implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer mid;
	private String mto;
	private String mfrom;
	private String msg;
	private String mdate;
	private Integer mstart;
	public Msg(Integer mid, String msg, String mdate, Integer mstart) {
		super();
		this.mid = mid;
		this.msg = msg;
		this.mdate = mdate;
		this.mstart = mstart;
	}
	
	public Msg() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getMid() {
		return mid;
	}
	public void setMid(Integer mid) {
		this.mid = mid;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public Integer getMstart() {
		return mstart;
	}
	public void setMstart(Integer mstart) {
		this.mstart = mstart;
	}

	public String getMto() {
		return mto;
	}

	public void setMto(String mto) {
		this.mto = mto;
	}

	public String getMfrom() {
		return mfrom;
	}

	public void setMfrom(String mfrom) {
		this.mfrom = mfrom;
	}
	
	
	
}
