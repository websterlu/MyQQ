package com.zsl.dao.impl;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.zsl.bean.Msg;
import com.zsl.dao.MessageDao;
import com.zsl.db.DBConnection;

public class MessageDaoImpl implements MessageDao {
	private DBConnection dbConnection=null;
	private SQLiteDatabase database=null;
	private Cursor cursor=null;
	
	
	public MessageDaoImpl(Context context) {
		super();
		this.dbConnection = new DBConnection(context);
		this.database = dbConnection.getWritableDatabase();
	}
	

	public MessageDaoImpl() {
		super();
		// TODO Auto-generated constructor stub
	}


	@Override
	public synchronized boolean save(Msg message) {
		// TODO Auto-generated method stub
		String sql="insert into msg(mto,mfrom,msg,mdate,mstart) values(?,?,?,?,?)";
		try {
			database.execSQL(sql,new  Object[]{message.getMto(),message.getMfrom(),message.getMsg(),message.getMdate(),message.getMstart()});
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean del(Msg message) {
		// TODO Auto-generated method stub
		try {
			database.delete("msg"," mto=? and mfrom=?",new String[]{message.getMto().toString(),message.getMfrom().toString()} );
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public synchronized boolean update(Msg message) {
		// TODO Auto-generated method stub
		
		try {
			String sql="UPDATE msg SET mstart=1 WHERE mto=? and mfrom=? and mstart=0";
			database.execSQL(sql, new Object []{message.getMto().toString(),message.getMfrom()});
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public synchronized List<Msg> findAll(Msg message) {
		// TODO Auto-generated method stub
		String sql="select * from msg where mto=? and mfrom=? and mstart=0";
		try {
			cursor=database.rawQuery(sql, new String[]{message.getMto().toString(),message.getMfrom().toString()});
			List<Msg> list=new ArrayList<Msg>();
			while(cursor.moveToNext()) {
				Msg msg=new Msg();
				msg.setMid(cursor.getInt(cursor.getColumnIndex("mid")));
				msg.setMto(cursor.getString(cursor.getColumnIndex("mto")));
				msg.setMfrom(cursor.getString(cursor.getColumnIndex("mfrom")));
				msg.setMsg(cursor.getString(cursor.getColumnIndex("msg")));
				msg.setMdate(cursor.getString(cursor.getColumnIndex("mdate")));
				msg.setMstart(cursor.getInt(cursor.getColumnIndex("mstart")));
				list.add(msg);
				
			}
			
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public synchronized int findAllStartCount(Msg message) {
		try {
			String sql="select count(*) from msg where mto=? and mfrom=? and mstart=0";
			cursor=database.rawQuery(sql,new String[]{message.getMto(),message.getMfrom()});
			if (cursor.moveToNext()) {
				return cursor.getInt(Integer.parseInt(cursor.getColumnName(0)));
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cursor.close();
		}
		
		return 0;
	}


	@Override
	public synchronized List<Msg> findAllByGroup(Msg message) {
		// TODO Auto-generated method stub
		List<Msg> list=new ArrayList<Msg>();
		String sql="select mto,mfrom,count(mfrom) as countmsg from msg where mto=? and mstart=0 group by mfrom";
		try {
			cursor=database.rawQuery(sql, new String[]{message.getMto().toString()});
			
			while(cursor.moveToNext()) {
				Msg msg=new Msg();
				msg.setMto(cursor.getString(cursor.getColumnIndex("mto")));
				msg.setMfrom(cursor.getString(cursor.getColumnIndex("mfrom")));
				msg.setMsg(cursor.getString(cursor.getColumnIndex("countmsg")));
				list.add(msg);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			cursor.close();
		}
		return list;
	}

}
