package com.zsl.dao;

import java.util.List;

import com.zsl.bean.Msg;

public interface MessageDao {
	boolean save(Msg message);
	boolean del(Msg message);
	boolean update(Msg message);
	List<Msg> findAll(Msg message);
	int findAllStartCount(Msg message);
	List<Msg> findAllByGroup(Msg msg);
}

