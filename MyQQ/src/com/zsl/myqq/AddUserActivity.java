package com.zsl.myqq;

import java.util.HashMap;
import java.util.List;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;

import com.zsl.smack.Smack;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent.OnFinished;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


/**
 * 添加User
 * @author ZSL
 * @version 2002-1-1 12:05:27
 */
public class AddUserActivity extends Activity {
	private Spinner spinner;
	private TextView tv_name;
	private TextView tv_rename;
	private Button bt_add;
	private ImageView top_iv_contants;
	private TextView top_tv_contants;
	private List<RosterGroup> rgList;
	private Smack smack=LoginActivity.getSmack();
	private String [] groups={};
	private ArrayAdapter<String> adapter;
	private Handler handler;
	private Dialog dialog;
	private LayoutInflater inflater;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_user);
		init();
		inflater=inflater.from(AddUserActivity.this);
		rgList=smack.getRosterGroup();
		int l=0;
		if (rgList.size()==0) {
			l=1;
		}else{
			l=rgList.size();
		}
		groups=new String[l];
		groups[0]="我的好友";
		for (int i = 0; i < rgList.size(); i++) {
			groups[i]=rgList.get(i).getName().toString();
			System.out.println(groups[i].toString());
		}
		adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,groups);
		spinner.setAdapter(adapter);
		top_iv_contants.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		top_tv_contants.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				
				switch (msg.what) {
				//验证土司
				case 0:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					String obj=msg.obj.toString();
					Toast.makeText(AddUserActivity.this,obj , Toast.LENGTH_SHORT).show();
					if (obj.equals("添加成功")) {
						
						handler.sendEmptyMessage(3);
					}
				}
				break;
				//查询好友提示
				case 1:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					dialog=new Dialog(AddUserActivity.this);
					dialog.setTitle("添加好友提示");
					RelativeLayout ll=(RelativeLayout) inflater.inflate(R.layout.toast, null);
					TextView tv=(TextView) ll.findViewById(R.id.toast_text);
					tv.setText("查询中...");
					dialog.setContentView(ll);
					dialog.show();
				}
				break;
				//添加提示
				case 2:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					dialog=new Dialog(AddUserActivity.this);
					dialog.setTitle("添加好友提示");
					RelativeLayout ll=(RelativeLayout) inflater.inflate(R.layout.toast, null);
					TextView tv=(TextView) ll.findViewById(R.id.toast_text);
					tv.setText("添加中...");
					dialog.setContentView(ll);
					dialog.show();
				}
				break;
				//添加提示
				case 3:
				{
					Intent intent=new Intent(AddUserActivity.this, TabMainActivity.class);
					startActivity(intent);
					AddUserActivity.this.finish();
				}
				break;
				default:
					break;
				}
			}
		};
		//添加好友
		bt_add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String name=tv_name.getText().toString();
				if ("".equals(name)||name.equals(null)) {
					Message msg=new Message();
					msg.what=0;
					msg.obj="账号不能为空哦，亲";
					handler.sendMessage(msg);
				}else{
//					handler.sendEmptyMessage(1);
//					List<HashMap<String, Object>> results=smack.searchUser(name);
//					if (results!=null) {
						handler.sendEmptyMessage(2);
						String rename=tv_rename.getText().toString();
						String groupName=spinner.getSelectedItem().toString();
						if(smack.addRoster(name, rename, groupName)){
							Message msg=new Message();
							msg.what=0;
							msg.obj="添加成功";
							handler.sendMessage(msg);
						}else{
							Message msg=new Message();
							msg.what=0;
							msg.obj="添加失败,请重新添加";
							handler.sendMessage(msg);
						}
//					}else{
//						Message msg=new Message();
//						msg.what=0;
//						msg.obj="好友账号不存在，亲";
//						handler.sendMessage(msg);
//					}
				}
			}
		});
	}
	/**
	 * 初始化
	 */
	public void init(){
		spinner=(Spinner) findViewById(R.id.add_user_spinner);
		tv_name=(TextView) findViewById(R.id.addUser_et_name);
		tv_rename=(TextView) findViewById(R.id.addUser_et_rename);
		bt_add=(Button) findViewById(R.id.addUser_bt_add);
		top_iv_contants=(ImageView) findViewById(R.id.addUser_top_iv_contants);
		top_tv_contants=(TextView) findViewById(R.id.addUser_top_tv_contants);
	}
	
	@Override
	public void onBackPressed() {
		Intent intent=new Intent(AddUserActivity.this, TabMainActivity.class);
		startActivity(intent);
		AddUserActivity.this.finish();
	}
	
}
