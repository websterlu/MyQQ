package com.zsl.myqq;

import org.jivesoftware.smack.util.StringUtils;

import com.zsl.smack.Smack;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * 添加组
 * @author ZSL
 * @version 2014-6-18 4:03:07
 */
public class AddGroupActivity extends Activity {
	private EditText et_gname;
	private Button bt_add;
	private ImageView iv_back;
	private Handler handler;
	private Dialog dialog;
	private LayoutInflater inflater;
	private Smack smack=LoginActivity.getSmack();
	Message msg=new Message();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_group);
		init();
		inflater=inflater.from(AddGroupActivity.this);
		iv_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		bt_add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String gname=et_gname.getText().toString();
				if (valdate(gname)) {
					handler.sendEmptyMessage(2);
					if(smack.addGroup(gname)){
						msg.obj="添加成功";
						msg.what=1;
						handler.sendMessage(msg);
					}else{
						msg.obj="添加失败，重新添加";
						msg.what=1;
						handler.sendMessage(msg);
					}
				}else{
					msg.obj="组名称不可以为空哦";
					msg.what=1;
					handler.sendMessage(msg);
				}
			}
		});
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					String content=msg.obj.toString();
					Toast.makeText(AddGroupActivity.this, content, Toast.LENGTH_SHORT).show();
					if (content.equals("添加成功")) {
						onBackPressed();
					}
				}
				break;
				//添加中提示
				case 2:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					dialog=new Dialog(AddGroupActivity.this);
					dialog.setTitle("添加好友提示");
					RelativeLayout ll=(RelativeLayout) inflater.inflate(R.layout.toast, null);
					TextView tv=(TextView) ll.findViewById(R.id.toast_text);
					tv.setText("查询中...");
					dialog.setContentView(ll);
					dialog.show();
				}
				break;

				default:
					break;
				}
			}
		};
	}
	/**
	 * 初始化
	 */
	public void init(){
		et_gname=(EditText) findViewById(R.id.add_group_et_name);
		bt_add=(Button) findViewById(R.id.add_group_bt_add);
		iv_back=(ImageView) findViewById(R.id.add_group_top_iv_tab_me);
	}
	/**
	 * 验证
	 */
	public boolean valdate(String name){
		if ("".equals(name)&&name.equals(null)) {
			return false;
		}
		return true;
	}
	@Override
	public void onBackPressed() {
		Intent intent=new Intent(AddGroupActivity.this, TabMainActivity.class);
		startActivity(intent);
		AddGroupActivity.this.finish();
	}
}
