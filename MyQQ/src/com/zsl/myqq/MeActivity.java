package com.zsl.myqq;

import com.zsl.smack.Smack;
import com.zsl.smack.SmackImpl;
import com.zsl.util.MySession;
import com.zsl.util.SharedConfig;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MeActivity extends Activity implements OnClickListener{
	private Button bt_exit_app;
	private RelativeLayout rl_vcard;
	private LayoutInflater inflater;
	private TextView tv_uname;
	private RelativeLayout rl_feed;
	private RelativeLayout rl_about;
	private RelativeLayout rl_clear;
	private Dialog dialog;
	private Smack smack=new LoginActivity().getSmack();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_me);
		init();
		MySession mySession=MySession.getMySession();
		String uname=(String) mySession.get("me_uname");
		tv_uname.setText(uname);
		bt_exit_app.setOnClickListener(this);
		inflater=LayoutInflater.from(this);
		rl_vcard.setOnClickListener(this);
		rl_feed.setOnClickListener(this);
		rl_about.setOnClickListener(this);
		rl_clear.setOnClickListener(this);
	}
	/**
	 * 初始化控件
	 */
	private void init(){
		bt_exit_app=(Button) findViewById(R.id.exit_app);	
		rl_vcard=(RelativeLayout) findViewById(R.id.tab_me_ll_vcard);
		tv_uname=(TextView) findViewById(R.id.tab_me_uname);
		rl_feed=(RelativeLayout) findViewById(R.id.tab_me_feed);
		rl_about=(RelativeLayout) findViewById(R.id.tab_me_rl_about);
		rl_clear=(RelativeLayout) findViewById(R.id.tab_me_clear);
		
		
	}
	
	/**
	 * 点击事件
	 */
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.exit_app:
		{
			LinearLayout linearLayout=(LinearLayout) inflater.inflate(R.layout.common_menu_dialog_2btn_layout, null);
			Button bt_exit=(Button) linearLayout.findViewById(R.id.btn_exit);
			Button bt_cancel=(Button) linearLayout.findViewById(R.id.btn_cancel);
			bt_exit.setText("确定退出");
			bt_exit.setOnClickListener(this);
			bt_cancel.setOnClickListener(this);
			dialog=new Dialog(MeActivity.this);
			dialog.setTitle("退出提示");
			dialog.setContentView(linearLayout);
			dialog.show();
		}
		break;
		//确认退出
		case R.id.btn_exit:{
			smack.logout();
			dialog.dismiss();
			this.finish();
			
		}
		break;
		//去个人中心
		case R.id.tab_me_ll_vcard:{
			Intent intent=new Intent(MeActivity.this, MeVcardActivity.class);
			startActivity(intent);
		}
		break;
		//取消
		case R.id.btn_cancel:{
			dialog.dismiss();
		}
		break;
		//反馈
		case R.id.tab_me_feed:{
			Intent intent=new Intent(MeActivity.this,FeedActivity.class );
			startActivity(intent);
		}
		
		break;
		//关于
		case R.id.tab_me_rl_about:{
			Intent intent=new Intent(MeActivity.this,AboutActivity.class );
			startActivity(intent);
		}
		break;
		//清除缓存
		case R.id.tab_me_clear:{
			new SharedConfig(this).ClearConfig();
			Toast.makeText(this, "清除成功", Toast.LENGTH_SHORT).show();
		}
		break;
		default:
			break;
		}
	};
	
}
