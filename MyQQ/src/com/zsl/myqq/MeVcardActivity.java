package com.zsl.myqq;

import com.zsl.util.MySession;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MeVcardActivity extends Activity {
	private RelativeLayout rl_changepwd;
	private ImageView iv_back;
	private TextView tv_uname;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mevcard);
		init();
		MySession mySession=MySession.getMySession();
		String uname=(String) mySession.get("me_uname");
		tv_uname.setText(uname);
		
		rl_changepwd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(MeVcardActivity.this, ChangePwdActivity.class);
				startActivity(intent);
				
			}
		});
		iv_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}
	
	public void init(){
		iv_back=(ImageView) findViewById(R.id.mevcard_top_iv_tab_me);
		rl_changepwd=(RelativeLayout) findViewById(R.id.mevcard_ll_changePwd);
		tv_uname=(TextView) findViewById(R.id.mecard_uname);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
}
