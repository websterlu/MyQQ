package com.zsl.myqq;


import com.zsl.smack.Smack;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * �޸�����ҳ��
 * @author ZSL
 * @version 
 */
public class ChangePwdActivity extends Activity {
	private ImageView iv_back;
	private EditText et_pwd;
	private EditText et_repwd;
	private Button bt_change;
	private Handler handler;
	private Smack smack=LoginActivity.getSmack();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changepwd);
		init();
		
		iv_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				onBackPressed();
			}
		});
		bt_change.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String pwd=et_pwd.getText().toString();
				String repwd=et_repwd.getText().toString();
				if (valdate(pwd)&&valdate(repwd)) {
					if (pwd.equals(repwd)) {
						boolean changed=smack.changePwd(pwd);
						if (changed) {
							handler.sendEmptyMessage(1);
						}else{
							handler.sendEmptyMessage(2);
						}
					}else{
						handler.sendEmptyMessage(3);
					}
				}else{
					handler.sendEmptyMessage(4);
				}
				
			}
		});
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
				case 1:
				{
					Toast.makeText(ChangePwdActivity.this, "�����޸ĳɹ��������µ�¼��", Toast.LENGTH_LONG).show();
					handler.sendEmptyMessageDelayed(5, 3000);
				}
				break;
				case 2:
				{
					Toast.makeText(ChangePwdActivity.this, "�����޸�ʧ�ܣ����Ժ��޸�", Toast.LENGTH_LONG).show();
				}
				break;
				case 3:
				{
					Toast.makeText(ChangePwdActivity.this, "���벻һ�£�", Toast.LENGTH_LONG).show();
				}
				break;
				case 4:
				{
					Toast.makeText(ChangePwdActivity.this, "���벻��Ϊ�գ�", Toast.LENGTH_LONG).show();
				}
				break;
				
				case 5:
				{
					new Thread(){
						public void run() {
							smack.getConnection().disconnect();
							Intent intent=new Intent(ChangePwdActivity.this, LoginActivity.class);
							startActivity(intent);
							
						};
						
					}.start();
					
				}
				break;
				
				default:
					break;
				}
			}
		};
		
	}
	
	/**
	 * ��ʼ��
	 */
	
	public void init(){
		iv_back=(ImageView) findViewById(R.id.changepwd_top_iv_tab_me);
		et_pwd=(EditText) findViewById(R.id.changepwd_et_pwd);
		et_repwd=(EditText) findViewById(R.id.changepwd_et_repwd);
		bt_change=(Button) findViewById(R.id.changepwd_bt_change);
	}
	
	/**
	 * ��֤
	 * @return
	 */
	
	public boolean valdate(String name){
		if ("".equals(name)||name.equals(null)) {
			return false;
		}
		return true;
	}
}
