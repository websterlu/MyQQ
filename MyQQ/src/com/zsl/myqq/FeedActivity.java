package com.zsl.myqq;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * 反馈
 * @author ZSL
 * @version 2014-6-22 10:09:46
 */

public class FeedActivity extends Activity {
	private  ImageView iv_back;
	private Button bt_feed;
	private Handler handler;
	private Dialog dialog;
	private LayoutInflater inflater;
	private EditText et_feed;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feed_back_view);
		init();
		inflater=inflater.from(FeedActivity.this);
		
		//返回方法
		iv_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
				FeedActivity.this.finish();
			}
		});
		//提交
		bt_feed.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String feed_content=et_feed.getText().toString();
				
				if ("".equals(feed_content)||feed_content.equals(null)) {
					handler.sendEmptyMessage(3);
				}else{
					
					handler.sendEmptyMessage(1);
				}
			}
		});
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				//显示提交
				case 1:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					dialog=new Dialog(FeedActivity.this);
					dialog.setTitle("提交提示");
					RelativeLayout ll=(RelativeLayout) inflater.inflate(R.layout.toast, null);
					TextView tv=(TextView) ll.findViewById(R.id.toast_text);
					tv.setText("提交中...");
					dialog.setContentView(ll);
					dialog.show();
					handler.sendEmptyMessageDelayed(2,1500);
				}
				break;
				//提交成功
				case 2:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(FeedActivity.this, "提交成功啦", Toast.LENGTH_LONG).show();
					onBackPressed();
					FeedActivity.this.finish();
				}
				break;
				//提示提交内容不可以为空
				case 3:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(FeedActivity.this, "说些什么吧，亲", Toast.LENGTH_LONG).show();
				}
				break;
				
				default:
					break;
				}
				
			}
		};
		
	}
	/**
	 * 初始化
	 */
	private void init(){
		bt_feed=(Button) findViewById(R.id.feed_btn_feed);
		iv_back=(ImageView) findViewById(R.id.feed_top_iv_tab_me);
		et_feed=(EditText) findViewById(R.id.feed_back_edit);
	};
}
